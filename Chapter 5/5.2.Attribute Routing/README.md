# 5.2 Attribute Routing

<!-- ttoc -->

## Attribute Routes
> Use RouteAttributes to decorate actions in your controllers. There is a specific RouteAtribute for each of the four HTTP verbs: GET, PUT, POST, and DELETE. _Each of these attributes comes in two flavors: one for MVC actions and one for Web API actions._<br/><br/>
-- From Section Quickstart

目前使用上覺得方變得一點是：
- 我們可以透過 Attribute Routes 達到兩個不同的 Get Method 使用同一個 URI。

## Settings

### WebApiConfig.cs

設定不會很複雜，確認 Web API 專案能否使用 Attribute Routes ，可以在 專案目錄「**_~/App_Start/WebApiConfig.cs_**」內檢查。

```csharp
namespace WebApplication1 {
    public class WebApiConfig {
        public static void Register(HttpConfiguration config) {
            // This is for Web API
            config.MapHttpAttributeRoutes(); // 檢查是不是有這個存在
        }
    }
}
```

### Global.asax.cs

再來，註冊 ASP.NET Web API 相關設定

在「 Global.asax.cs 」檔案最上方引用 System.Web.Http 命名空間

```csharp
using System.Web.Http;
```

在 Application_Start() 方法中加入以下程式碼：

```csharp
GlobalConfiguration.Configure(WebApiConfig.Register);
```

#### 讓回應是 Json Format
> Refer to [如何讓 ASP.NET Web API 無論任何要求都回應 JSON 格式](http://blog.miniasp.com/post/2012/10/12/ASPNET-Web-API-Force-return-JSON-format-instead-of-XML-for-Google-Chrome-Firefox-Safari.aspx)

一樣是在「 Global.asax.cs 」檔案中的Application_Start() 方法內的最後面加上以下程式碼：

```csharp
GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
```
##### Also Read
- Read section [**JSON only responses**] In [Best Practices for Designing a Pragmatic RESTful API](http://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api)

## Samples

假設聯絡人是這個樣子：

```csharp
Contact[] contacts = new Contact[]
        {
            new Contact() { Id = 0,FirstName = "Peter", LastName = "Parker"},
            new Contact() { Id = 1,FirstName = "Bruce", LastName = "Wayne"},
            new Contact() { Id = 2,FirstName = "Bruce", LastName = "Banner"}
        };
```

### Get Method

#### 取得所有聯絡人資料
```csharp
[Route("api/Contact")]
public IEnumerable<Contact> Get() {
  return contacts;
}
```

#### 透過 Id 取得聯絡人
```csharp
[Route("api/Contact/{id:int}")]
public IHttpActionResult Get(int id) {
    Contact contact = contacts.FirstOrDefault<Contact>(c => c.Id == id);

    if (contact == null) {
        return NotFound();
    }

    return Ok(contact);
}
```
> 這邊的 int 屬於一種 **[Route Constraints](https://www.asp.net/web-api/overview/web-api-routing-and-actions/attribute-routing-in-web-api-2#constraints)**，在下一個章節會提到。

#### 透過 Name 取得聯絡人
```csharp
[Route("api/Contact/{name}")]
[HttpGet]
public IEnumerable<Contact> FindContactByName(string name) {
    Contact[] contactArray = contacts.Where<Contact>(c => c.FirstName.Contains(name)).ToArray<Contact>();

    return contactArray;
}
```

## More References
- [Attribute Routing in ASP.NET Web API 2](https://www.asp.net/web-api/overview/web-api-routing-and-actions/attribute-routing-in-web-api-2)
- [Using HTTP Methods (GET, POST, PUT, etc.) in Web API](https://www.exceptionnotfound.net/using-http-methods-correctly-in-asp-net-web-api/)
- [Best Practices for Designing a Pragmatic RESTful API](http://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api)
- [如何讓 ASP.NET Web API 無論任何要求都回應 JSON 格式](http://blog.miniasp.com/post/2012/10/12/ASPNET-Web-API-Force-return-JSON-format-instead-of-XML-for-Google-Chrome-Firefox-Safari.aspx)
- [ASP.NET MVC - 使用 Attribute Routing](http://kevintsengtw.blogspot.tw/2013/12/aspnet-mvc-attribute-routing.html)
- [mccalltd/AttributeRouting @ GitHub](https://github.com/mccalltd/AttributeRouting/wiki)
