# 5.3 Route Prefixes & Constraints

<!-- ttoc -->

可能會使用到的地方：
- 在 API 方法中需要限制參數，就可以使用到 **[Route Constraints](https://www.asp.net/web-api/overview/web-api-routing-and-actions/attribute-routing-in-web-api-2#constraints)**
- 在沒有特殊需求的情況下，在同一個 API Controller 內應該都會是在同一個 **[Route Prefixes](https://www.asp.net/web-api/overview/web-api-routing-and-actions/attribute-routing-in-web-api-2#prefixes)** 下做變化

## Route Constraints

General Syntax 是 **"{parameter:constraint}"** 。

- 限制輸入參數最小值是 1 ，並且是 Integer。
```csharp
[Route("api/Contact/{id:int:min(1)}")]
public IHttpActionResult Get(int id) {
    Contact contact = contacts.FirstOrDefault<Contact>(c => c.Id == id);

    if (contact == null) {
        return NotFound();
    }

    return Ok(contact);
}
```

### More Info
- [Route Constraints](https://www.asp.net/web-api/overview/web-api-routing-and-actions/attribute-routing-in-web-api-2#constraints)

## Route Prefixes

原本 API Controller 是這個樣子
```csharp
public class ContactController : ApiController {
    Contact[] contacts = new Contact[] {
        new Contact() { Id = 0,FirstName = "Peter", LastName = "Parker"}
    };
    [Route("api/Contact")]
    public IEnumerable<Contact> Get() {
        return contacts;
    }
    [Route("api/Contact/{id:int}")]
    public IHttpActionResult Get(int id) {
        Contact contact = contacts.FirstOrDefault<Contact>(c => c.Id == id);

        if (contact == null)    return NotFound();

        return Ok(contact);
    }

    [Route("api/Contact/{name}")]
    [HttpGet]
    public IEnumerable<Contact> FindContactByName(string name) {
        Contact[] contactArray = contacts.Where<Contact>(c => c.FirstName.Contains(name)).ToArray<Contact>();

        return contactArray;
    }
}
```

可以發現在 Controller 內每個方法都是有同樣的前綴，
這個時候就能使用到 **Route Prefixes** 來簡化 Attribute Routing。

將所有相同的「**api/Contact**」搬到整個 Controller Class 的外面，並且加上「**[RoutePrefix("api/Contact")]**」。

以下程式碼為修改後
```csharp
[RoutePrefix("api/Contact")]
public class ContactController : ApiController {
    Contact[] contacts = new Contact[] {
        new Contact() { Id = 0,FirstName = "Peter", LastName = "Parker"}
    };
    [Route("")]
    public IEnumerable<Contact> Get() {
        return contacts;
    }
    [Route("{id:int}")]
    public IHttpActionResult Get(int id) {
        Contact contact = contacts.FirstOrDefault<Contact>(c => c.Id == id);

        if (contact == null)    return NotFound();

        return Ok(contact);
    }

    [Route("{name}")]
    [HttpGet]
    public IEnumerable<Contact> FindContactByName(string name) {
        Contact[] contactArray = contacts.Where<Contact>(c => c.FirstName.Contains(name)).ToArray<Contact>();

        return contactArray;
    }
}
```

### More Info
- [Route Prefixes](https://www.asp.net/web-api/overview/web-api-routing-and-actions/attribute-routing-in-web-api-2#prefixes)

## More References
- [Attribute Routing in ASP.NET Web API 2](https://www.asp.net/web-api/overview/web-api-routing-and-actions/attribute-routing-in-web-api-2)
