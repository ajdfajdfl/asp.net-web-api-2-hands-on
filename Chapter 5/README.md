# Chapter 5
> URI customization & Entity Framework integration

* [5.2 - Attribute Routing](5.2.Attribute Routing/README.md)
* [5.3 - Route Prefixes & Constraints](5.3.Route Prefixes And Constraints/README.md)
